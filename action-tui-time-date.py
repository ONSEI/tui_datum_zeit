#!/usr/bin/env python3

from snipsTools import SnipsConfigParser
from hermes_python.hermes import Hermes
import datetime
from dateutil.parser import parse
import requests

CONFIG_INI = "config.ini"

MQTT_IP_ADDR = "localhost"
MQTT_PORT = 1883
MQTT_ADDR = "{}:{}".format(MQTT_IP_ADDR, str(MQTT_PORT))
USERNAME_INTENTS = "jsemig"
SHIP_DATA_URL = 'https://tuic-ms3-api.prelive.cellular.de/cruise/data/time'


class TuiTimeAndDate(object):
    """Class used to wrap action code with mqtt connection

        Please change the name refering to your application
    """

    def __init__(self):
        # get the configuration if needed
        try:
            self.config = SnipsConfigParser.read_configuration_file(CONFIG_INI)
        except:
            self.config = None

        # start listening to MQTT
        self.start_blocking()

    @staticmethod
    def user_intent(intentname):
        return USERNAME_INTENTS + ":" + intentname

    @staticmethod
    def get_ship_time() -> datetime:
        r = requests.get(SHIP_DATA_URL)
        if r.status_code < 300:
            return parse(r.json()['dateTime'])

    def current_time(self, hermes, intent_message):
        ship_time = self.get_ship_time()
        hours = ship_time.hour
        minutes = ship_time.minute
        if minutes == 0:
            minutes = ""
        if hours == 1:
            result_sentence = "Gerade ist es ein Uhr {0} .".format(minutes)
        else:
            result_sentence = "Gerade ist es {0} Uhr {1} .".format(hours, minutes)
        current_session_id = intent_message.session_id
        hermes.publish_end_session(current_session_id, result_sentence)


    def current_date(self, hermes, intent_message):
        ship_time = self.get_ship_time()
        year = ship_time.year
        month = ship_time.month
        day = ship_time.day
        weekday = ship_time.isoweekday()
        weekday_list = ['Montag', 'Dienstag', 'Mittwoch', 'Donnerstag', 'Freitag', 'Samstag', 'Sonntag']
        month_list = ['Januar', 'Februar', 'März', 'April', 'Mai', 'Juni',
                      'Juli', 'August', 'September', 'Oktober', 'November', 'Dezember']

        result_sentence = "Heute ist {0}, der {1} {2} {3}"\
            .format(weekday_list[weekday - 1], day, month_list[month - 1], year)
        current_session_id = intent_message.session_id
        hermes.publish_end_session(current_session_id, result_sentence)

    # --> Master callback function, triggered everytime an intent is recognized
    def master_intent_callback(self, hermes, intent_message):
        coming_intent = intent_message.intent.intent_name
        if coming_intent == self.user_intent('currentTime'):
            self.current_time(hermes, intent_message)
        elif coming_intent == self.user_intent('currentDate'):
            self.current_date(hermes, intent_message)

    # --> Register callback function and start MQTT
    def start_blocking(self):
        with Hermes(MQTT_ADDR) as h:
            h.subscribe_intents(self.master_intent_callback).start()


if __name__ == "__main__":
    TuiTimeAndDate()
